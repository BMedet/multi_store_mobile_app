import { boot } from 'quasar/wrappers';
import  vClickOutside  from  'click-outside-vue3';


export default boot(({ app }) => {
    app.use(vClickOutside);
});
