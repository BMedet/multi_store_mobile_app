/**
 * @author Medet Bakhytzhan <b.m.bakytzhan@gmail.com>
 */

let backendBaseUrl = process.env.LOCAL_BACKEND_HOST;
switch(process.env.APP_ENV) {
    case 'dev':
        backendBaseUrl = process.env.DEV_BACKEND_HOST;
        break;
    case 'production':
        backendBaseUrl = process.env.MASTER_BACKEND_HOST;
        break;
}

const appConfig = {
    backendBaseUrl,
    storeId: process.env.STORE_ID,
    media: {
        plugImage: 'https://marwin.kz/media/catalog/product/placeholder/default/Selection_01222_8.png',
    },
}

export default appConfig;