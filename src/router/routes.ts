import { RouteRecordRaw } from 'vue-router';
import HomePage from 'src/pages/Home/HomePage.vue';
import SearchPage from 'src/pages/Search/SearchPage.vue';
import CatalogPage from 'src/pages/Catalog/Index.vue';
import CheckoutPage from 'src/pages/Checkout/CheckoutPage.vue';
import WishlistPage from 'src/pages/Wishlist/WishlistPage.vue';
import AccountPage from 'src/pages/Account/AccountPage.vue';
import MainLayout from 'src/layouts/MainLayout.vue';
import CategoriesTree from 'src/pages/Catalog/CategoriesTree/CategoriesTreePage.vue';
import CategoryPage from 'src/pages/Catalog/Category/CategoryPage.vue';
import ProductPage from 'src/pages/Catalog/Product/ProductPage.vue';
import ErrorPage from 'src/pages/Error/ErrorPage.vue';

const routes: RouteRecordRaw[] = [
    {
        path: '',
        component: MainLayout,
        name: 'main_layout',
        redirect: { name: 'home_page' },
        children: [
            {
                path: 'home',
                name: 'home_page',
                component: HomePage
            },
            {
                path: 'search',
                name: 'search_page',
                component: SearchPage
            },
            {
                path: 'catalog',
                name: 'catalog_page',
                component: CatalogPage,
                redirect: { name: 'categories_tree_page' },
                children: [
                    {
                        path: '',
                        name: 'categories_tree_page',
                        component: CategoriesTree,
                    },
                    {
                        path: 'category/:id', 
                        name: 'category_page',
                        component: CategoryPage
                    },
                    {
                        path: 'product/:productId', 
                        name: 'product_page',
                        component: ProductPage
                    }
                ]
            },
            {
                path: 'checkout',
                name: 'checkout_page',
                component: CheckoutPage
            },
            {
                path: 'wishlist',
                name: 'wishlist_page',
                component: WishlistPage
            },
            {
                path: 'account',
                name: 'account_page',
                component: AccountPage
            },
            {
                path: 'error',
                name: 'error_page',
                component: ErrorPage
            }
        ],
    },

    // Always leave this as last one,
    // but you can also remove it
    {
        path: '/:catchAll(.*)*',
        component: () => import('pages/ErrorNotFound.vue'),
    },
    
];

export default routes;
