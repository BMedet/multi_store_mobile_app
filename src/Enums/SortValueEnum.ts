/**
 * @author Medet Bakhytzhan <b.m.bakytzhan@gmail.com>
 */

enum SortValueEnum {

    asc = 'asc',
    desc = 'desc',
}
export default SortValueEnum;