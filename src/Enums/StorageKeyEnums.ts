/**
 * @author Medet Bakhytzhan <b.m.bakytzhan@gmail.com>
 */

enum StorageKeyEnums {

    // Region
    regionOptionsKey = 'region_options',

    // Catalog category
    catalogCategoryTree = 'catalog_category_tree',
    catalogCategory = 'catalog_category',

    // Customer
    customer = 'customer'
}
export default StorageKeyEnums;