/**
 * @author Medet Bakhytzhan <b.m.bakytzhan@gmail.com>
 */

enum ListingTypeEnum {

    grid = 'grid',
    list = 'list',
}
export default ListingTypeEnum;