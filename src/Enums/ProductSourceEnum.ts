/**
 * @author Medet Bakhytzhan <b.m.bakytzhan@gmail.com>
 */

enum ProductSourceEnum {

    catalogCategory = 'category',
}
export default ProductSourceEnum;