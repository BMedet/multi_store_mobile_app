/**
 * @author Medet Bakhytzhan <b.m.bakytzhan@gmail.com>
 */

enum ProductSortEnum {

    bestseller = 'bestseller',
    date = 'magentoCreatedAt',
    priceUp = 'priceUp',
    priceDown = 'priceDown',
    discount = 'discount',
}
export default ProductSortEnum;