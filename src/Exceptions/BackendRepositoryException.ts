/**
 * @author Medet Bakhytzhan <b.m.bakytzhan@gmail.com>
 */

import { AxiosError } from 'axios';
import { routerState } from 'src/states/RouterState';

export default class BackendRepositoryException {

    protected frontendMessage: string;
    protected url: string;
    protected message: string;
    protected code?: string;
    protected httpCode: number;
    protected status: string;

    /**
     * Constructor
     */
    constructor(url: string, response: AxiosError, frontendMessage: string) {
        this.url = url;
        this.frontendMessage = frontendMessage;
        this.message = response.message;
        this.code = response.code;
        this.httpCode = response.response ? response.response.status : 0;
        this.status = response.response ? response.response.statusText : '';

        this.handle();
    }

    /**
     * Handle error
     */
    protected handle() {

        // Prepare params
        const preparedParams = {  //@todo typecast
            url: this.url,
            frontendMessage: this.frontendMessage,
            message: this.message,
            code: this.code,
            httpCode: this.httpCode,
            status: this.status
        }

        // Console log
        console.error(preparedParams);

        routerState.value.push({ name: 'error_page', params: preparedParams });

    }
}
