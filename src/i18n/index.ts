import enUS from './en-US';
import ruRU from './ru-RU';
import kkKz from './kk-KZ';

export default {
  'en-US': enUS,
  'ru-RU': ruRU,
  'kk-KZ': kkKz
};
