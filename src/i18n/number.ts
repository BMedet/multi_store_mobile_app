
export default {
    'ru-RU': {
        currency: {
            style: 'currency',
            currency: 'USD'
        }
    },
    'kz-KK': {
        currency: {
            style: 'currency',
            currency: 'JPY',
            currencyDisplay: 'symbol'
        }
    }
};
