// This is just an example,
// so you can safely delete all default props below



export default {
    Home: 'TEST',
    Catalog: 'Katalog',
    Checkout: 'Корзина',
    Wishlist: 'Избранное',
    Account: 'Аккаунт',
    Search: 'Поиск',
    Region: 'Qala',
    Language: 'Tili',
    Theme: 'Тема',
    Back: 'Artka',
    Light: 'Awyk',
    Dark: 'Temnaya',
    Russian: 'Oryssha',
    Kazakh: 'Qazaqsha',
    Казахский: 'Qazaqsha'
};
