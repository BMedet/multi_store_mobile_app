import { isNull } from 'lodash';
import { Ref, ref } from 'vue';

// defaultValues
const defaultTitle = null;
const defaultSearchForm = false;
const defaultLogo = false;
const hideHeader = ref(false);
const headerZ6000 = ref(false);

// States
const title: Ref<string | null> = ref(defaultTitle);
const searchForm: Ref<boolean> = ref(defaultSearchForm);
const logo: Ref<boolean> = ref(defaultLogo);

/**
 * Set states
 */
const setHeaderStates = (titleValue: string | null = null, canShowSearchForm = false, canShowLogo = false) => {

    if (canShowLogo) {
        canShowSearchForm = false;
    }

    if (!isNull(titleValue)) {
        canShowSearchForm = false;
        canShowLogo = false;
    }
    title.value = titleValue;
    searchForm.value = canShowSearchForm;
    logo.value = canShowLogo;

}
export { setHeaderStates, title, searchForm, logo, hideHeader, headerZ6000 };