import lodash from 'lodash';
import appConfig from 'src/config/app';
import { currentLanguageModel, initCurrentLanguage } from './LanguageService';
import { initCurrentRegion, initRegionOptions } from './RegionService';
import { initTheme } from './ThemeService';
import CustomerRepository from 'src/Repository/Backend/CustomerRepository';
import { useI18n } from 'vue-i18n';
import { initCategoriesTree } from '../Catalog/CatalogCategoryService';
import { SessionStorage } from 'quasar';
import StorageKeyEnums from 'src/Enums/StorageKeyEnums';
import CustomerInitType from 'src/Types/Customer/CustomerInitType';
import InitType from 'src/Types/Customer/InitType';

const customerRepository = new CustomerRepository();

/**
 * Get customer data from backend
 */
const initCustomerData = async (): Promise<void> => {

    // Set locale (kz | ru)
    const { locale } = useI18n();

    // Prepare customer data
    const customer = {
        storeId: appConfig.storeId,
        mobileUuid: lodash.get(window.device, 'uuid', null),
        platform: lodash.get(window.device, 'platform', null),
        platformVersion: lodash.get(window.device, 'version', null),
        model: lodash.get(window.device, 'model', null),
        manufacturer: lodash.get(window.device, 'manufacturer', null),
        serial: lodash.get(window.device, 'serial', null),
        sdkVersion: lodash.get(window.device, 'sdk_version', null),
        cordovaVersion: lodash.get(window.device, 'cordova_version', null)
    }

    // Make request and get init data
    const customerInitData = await customerRepository.initCustomer(customer);

    // Set region options to session storage
    initRegionOptions(customerInitData.regionOptions);

    // Init categories tree
    initCategoriesTree(customerInitData.categoryTree);

    // Init data to app
    initCustomerToApp(customerInitData);

    locale.value = currentLanguageModel.value.value;
}


/**
 * Update customer data
 */
const updateCustomerData = async (customerData: CustomerInitType) => {

    // Send to backend
    const customerInitData = await customerRepository.updateCustomerData(customerData);

    console.log(customerInitData)
    // Init data to app
    initCustomerToApp(customerInitData);
}

// Init to app
const initCustomerToApp = (customerInitData: InitType) => {

    // Set current theme
    initTheme(customerInitData.customerData.theme);

    // init current region
    initCurrentRegion(customerInitData.customerData.regionId)

    // Set current theme
    initCurrentLanguage(customerInitData.customerData.language);

    // Set to session storage
    SessionStorage.set(StorageKeyEnums.customer, customerInitData.customerData);
}


export { initCustomerData, updateCustomerData }

