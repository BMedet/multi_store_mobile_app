import { isNull } from 'lodash';
import { Dark, SessionStorage } from 'quasar';
import StorageKeyEnums from 'src/Enums/StorageKeyEnums';
import CustomerInitType from 'src/Types/Customer/CustomerInitType';
import { ref, Ref } from 'vue';
import { updateCustomerData } from './CustomerService';

// Init translate
type ThemeType = { label: string, value: string };

/**
 * States
 */
const currentThemeModel: Ref<ThemeType | null> = ref(null);
const themeOptions: ThemeType[] = [
    { label: 'Light', value: 'light' },
    { label: 'Dark', value: 'dark' },
];

const defaultTheme: ThemeType = themeOptions[0];

/**
 * Init from backend
 */
const initTheme = (themeCode: string): void => {
    for (const option of themeOptions) {
        if(option.value != themeCode) continue;
        currentThemeModel.value = option;
        break;
    }
    Dark.set(currentThemeModel.value?.value === 'dark')
}

/**
 * Set current theme
 */
const setCurrentTheme = async(): Promise<void> => {
    Dark.set(currentThemeModel.value?.value === 'dark'); // or false or "auto"
    if (isNull(currentThemeModel.value)) {
        currentThemeModel.value = defaultTheme;
    }

    // Get customer data from local storage
    const customerData = SessionStorage.getItem(StorageKeyEnums.customer) as CustomerInitType;

    // Modify customer data
    customerData.theme = currentThemeModel.value.value;

    // Send to backend
    await updateCustomerData(customerData);
}


export { initTheme, setCurrentTheme, currentThemeModel, themeOptions }

