import RegionType from '../../Types/RegionType';
import { SessionStorage } from 'quasar'
import { isNull } from 'lodash';
import StorageKeyEnums from 'src/Enums/StorageKeyEnums';
import { Ref, ref } from 'vue';
import RegionOptionType from 'src/Types/Region/RegionOptionType';
import CustomerInitType from 'src/Types/Customer/CustomerInitType';
import { updateCustomerData } from './CustomerService';

// Props
const currentRegionModel: Ref<RegionType | null> = ref(null);
const regionOptionsModel: Ref<RegionType[] | null> = ref(null);



/**
 * Init region options from backend
 */
const initRegionOptions = (regionOptions: RegionOptionType[]): void => {
    // Typecast
    const preparedRegions = [];
    for (const region of regionOptions) {
        const perRegion: RegionType = {
            label: region.name,
            value: region.entityId
        }
        preparedRegions.push(perRegion);
    }

    // Set to session storage
    SessionStorage.set(StorageKeyEnums.regionOptionsKey, preparedRegions);

    // Set state
    regionOptionsModel.value = preparedRegions;
}

/**
 * Init current region from backend
 */
const initCurrentRegion = (currentRegionId: number): void => {
    if (isNull(regionOptionsModel.value)) return;

    for (const region of regionOptionsModel.value) {
        if (region.value !== currentRegionId) continue;
        currentRegionModel.value = region;
        break;
    }
}

/**
 * Get current region
 */
const setCurrentRegion = async (): Promise<void> => {

    // Get customer data from local storage
    const customerData = SessionStorage.getItem(StorageKeyEnums.customer) as CustomerInitType;

    if (isNull(currentRegionModel.value)) return;
    // Modify customer data
    customerData.regionId = currentRegionModel.value.value;

    // Send to backend
    await updateCustomerData(customerData);
}

export {
    initRegionOptions,
    initCurrentRegion,
    setCurrentRegion,
    currentRegionModel,
    regionOptionsModel
}

