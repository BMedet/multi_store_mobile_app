import { Ref, ref } from 'vue';
import { SessionStorage } from 'quasar';
import StorageKeyEnums from 'src/Enums/StorageKeyEnums';
import CustomerInitType from 'src/Types/Customer/CustomerInitType';
import { updateCustomerData } from './CustomerService';

type LanguageType = {
    label: string,
    value: string
}

/**
 * States
 */
const languageOptions = [
    { label: 'Russian', value: 'ru-RU' },
    { label: 'Kazakh', value: 'kk-KZ' },
];

const currentLanguageModel: Ref<LanguageType> = ref(languageOptions[0]);

/**
 * Init current language
 */
const initCurrentLanguage = (currentLanguageCode: string): void => {

    for (const option of languageOptions) {
        if (option.value != currentLanguageCode) continue;
        currentLanguageModel.value = option;
        break;
    }
}

/**
 * Set current language
 */
const setCurrentLanguage = async (): Promise<void> => {

    // Get customer data from local storage
    const customerData = SessionStorage.getItem(StorageKeyEnums.customer) as CustomerInitType;

    // Modify customer data
    customerData.language = currentLanguageModel.value.value;

    // Send to backend
    await updateCustomerData(customerData);
}

export { initCurrentLanguage, setCurrentLanguage, currentLanguageModel, languageOptions }

