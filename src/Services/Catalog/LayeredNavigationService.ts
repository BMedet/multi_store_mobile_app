/**
 * @author Medet Bakhytzhan <b.m.bakytzhan@gmail.com>
 */

import ListingTypeEnum from 'src/Enums/ListingTypeEnum';
import ProductSortEnum from 'src/Enums/ProductSortEnum';
import ProductSourceEnum from 'src/Enums/ProductSourceEnum';
import { ref, Ref } from 'vue';
import { getProducts } from './CatalogProductService';
import { showLoader, hideLoader } from 'src/states/Loader';

const productListSortField: Ref<ProductSortEnum> = ref(ProductSortEnum.bestseller);
const listingType: Ref<ListingTypeEnum> = ref(ListingTypeEnum.grid);

const canShowFilterSidebar: Ref<boolean> = ref(false);
const applyTimeout = ref();

const filterDefaultValue = {
    price: {
        min: 0,
        max: 0
    },
    attribute: {} as any,
    stock: ['all']
};

const filterModel = ref(filterDefaultValue);

const sorterOptions = [
    {
        label: 'Bestseller',
        value: ProductSortEnum.bestseller,
        icon: 'mail',
    },
    {
        label: 'Novelty',
        value: ProductSortEnum.date,
        icon: 'mail',
    },
    {
        label: 'Price (Up)',
        value: ProductSortEnum.priceUp,
        icon: 'mail',
    },
    {
        label: 'Price (Down)',
        value: ProductSortEnum.priceDown,
        icon: 'mail',
    },
    {
        label: 'Discount',
        value: ProductSortEnum.discount,
        icon: 'mail',
    },
];
const sorterModel = ref(sorterOptions[0]);

const changeListingType = () => {
    listingType.value =
        listingType.value === ListingTypeEnum.grid
            ? ListingTypeEnum.list
            : ListingTypeEnum.grid;
};

/**
 * Flush filter
 */
const flushFilter = () => {
    filterModel.value = filterDefaultValue;
}

/**
 * Flush sorter
 */
const flushSorter = () => {
    sorterModel.value = sorterOptions[0];
    productListSortField.value = sorterModel.value.value;
}
// Apply filter
const applyLayeredNavigation = async (source: ProductSourceEnum, sourceId: number) => {

    clearTimeout(applyTimeout.value);

    applyTimeout.value = setTimeout(async () => {

        // Show loader
        showLoader();

        // Update product list
        await getProducts(
            source,
            sourceId,
            1,
            true
        );

        // Hide loader
        hideLoader();

    }, 1000);

};


// Current product model
export {
    flushSorter,
    flushFilter,
    productListSortField,
    listingType,
    filterModel,
    canShowFilterSidebar,
    sorterModel,
    sorterOptions,
    changeListingType,
    applyLayeredNavigation
}

