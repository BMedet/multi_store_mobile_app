/**
 * @author Medet Bakhytzhan <b.m.bakytzhan@gmail.com>
 */

import { Ref, ref } from 'vue';
import CatalogProductRepository from 'src/Repository/Backend/CatalogProductRepository';
import CatalogProductSearchResultType from 'src/Types/Catalog/CatalogProductSearchResultType';
import CatalogProductType from 'src/Types/Catalog/CatalogProductType';
import ProductSourceEnum from 'src/Enums/ProductSourceEnum';
import ProductSortEnum from 'src/Enums/ProductSortEnum';
import SortValueEnum from 'src/Enums/SortValueEnum';
import lodash from 'lodash';
import { filterModel, flushFilter, flushSorter, productListSortField } from './LayeredNavigationService';

const productsDefaultModel = {
    totalCount: 0,
    count: 0,
    page: 1,
    products: [],
    sort: {
        field: ProductSortEnum.bestseller,
        value: SortValueEnum.desc
    },
    aggregation: {
        minPrice: 0,
        maxPrice: 0
    },
    filterableAttributes: [],
    filter: []
};

// Props
const currentSourceProductsModel: Ref<CatalogProductSearchResultType> = ref(lodash.cloneDeep(productsDefaultModel));

const currentSource: Ref<ProductSourceEnum> = ref(ProductSourceEnum.catalogCategory);
const currentSourceId: Ref<number> = ref(0);

const currentProductModel: Ref<CatalogProductType | null> = ref(null);

/**
 * Constructor
 */
const productRepository: CatalogProductRepository = new CatalogProductRepository();

/**
 * Get products
 */
const getProducts = async (source: ProductSourceEnum, id: number, page = 1, filterApply = false): Promise<void> => {

    // Flush currentSourceProductsModel result on change page url
    if (currentSource.value != source || filterApply || currentSourceId.value !== id) {
        currentSourceProductsModel.value.products = [];
        currentSourceProductsModel.value.totalCount = .5;
    }

    // Flush filter on change page url
    if (currentSource.value != source || currentSourceId.value !== id) {
        flushFilter();
        flushSorter();
    }

    // Make request
    const data = await productRepository.getProducts(source, id, productListSortField.value, page, filterModel.value);

    // Set propertiers
    currentSourceProductsModel.value.totalCount = data.totalCount;
    currentSourceProductsModel.value.count = data.count;
    currentSourceProductsModel.value.page = data.page;
    currentSourceProductsModel.value.sort = data.sort;
    currentSourceProductsModel.value.products = currentSourceProductsModel.value.products.concat(data.products);
    currentSourceProductsModel.value.aggregation = data.aggregation;
    currentSourceProductsModel.value.filterableAttributes = data.filterableAttributes;
    filterModel.value = data.filter;
    if (!filterModel.value.price.max) {
        filterModel.value.price.max = data.aggregation.maxPrice;
    }
    
    if (!filterModel.value.price.min) {
        filterModel.value.price.min = data.aggregation.minPrice;
    }

    currentSource.value = source;
    currentSourceId.value = id;
}

/**
 * Get product
 */
const getProductById = async (productId: number): Promise<void> => {
    // @todo Set to session storage and get from session storage
    if (currentProductModel.value !== null) {
        return;
    }

    // Make request
    const productFromBackend = await productRepository.getProductById(productId);
    currentProductModel.value = productFromBackend;
    console.log(currentProductModel.value)
}

export {
    currentSourceProductsModel,
    getProducts,
    productListSortField,
    getProductById,
    currentProductModel,
    filterModel,
    currentSource,
    currentSourceId
}

