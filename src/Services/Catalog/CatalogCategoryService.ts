import { SessionStorage } from 'quasar'
import StorageKeyEnums from 'src/Enums/StorageKeyEnums';
import { Ref, ref } from 'vue';
import CatalogCategoryType from 'src/Types/Catalog/CatalogCategoryType';

// Props
const categoryTreeModel: Ref<any> = ref(null);
const currentCatalogCategoryModel: Ref<CatalogCategoryType | null> = ref(null);

/**
 * Set categories tree from backend
 */
const initCategoriesTree = (categoriesTree: any): void => {
    categoryTreeModel.value = categoriesTree;
    SessionStorage.set(StorageKeyEnums.catalogCategoryTree, categoryTreeModel.value);
}

/**
 * Get current category
 */
const getCurrentCategory = async (categoryId: number): Promise<void> => {
    // Prepare key
    const key = `${StorageKeyEnums.catalogCategoryTree}_${categoryId}`;

    // Get current catalog category from session storage
    const currentCatalogCategoryFromSessionStorage = SessionStorage.getItem(key);

    // If isset in session storage
    if (currentCatalogCategoryFromSessionStorage) {
        currentCatalogCategoryModel.value = currentCatalogCategoryFromSessionStorage as CatalogCategoryType;
        return;
    }

    // Set from categories tree
    for (const firstLvlCategory of categoryTreeModel.value) {
        // First level
        if (firstLvlCategory.entityId === categoryId) {
            currentCatalogCategoryModel.value = firstLvlCategory;
            continue;
        }

        // Second level
        for (const secondLvlCategory of firstLvlCategory.children) {
            if (secondLvlCategory.entityId === categoryId) {
                currentCatalogCategoryModel.value = secondLvlCategory;
                continue;
            }

            // Third level
            for (const thirdLvlCategory of secondLvlCategory.children) {
                if (thirdLvlCategory.entityId === categoryId) {
                    currentCatalogCategoryModel.value = thirdLvlCategory;
                    continue;
                }
            }
        }
    }

    // Set to session storage
    SessionStorage.set(key, currentCatalogCategoryModel.value);
}



export {
    initCategoriesTree,
    categoryTreeModel,
    getCurrentCategory,
    currentCatalogCategoryModel
}

