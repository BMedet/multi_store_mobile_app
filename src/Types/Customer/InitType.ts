import CatalogCategoryType from '../Catalog/CatalogCategoryType';
import RegionOptionType from '../Region/RegionOptionType';
import CustomerInitType from './CustomerInitType';

export default interface InitType {
    customerData: CustomerInitType;
    regionOptions: RegionOptionType[];
    categoryTree: CatalogCategoryType[];
}
