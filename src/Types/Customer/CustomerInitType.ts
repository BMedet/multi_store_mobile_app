export default interface CustomerInitType {
    customerId: number;
    mobileUuid: string;
    platform: string | null;
    platformVersion: string | null;
    model: string | null;
    manufacturer: string | null;
    serial: string | null;
    sdkVersion: string | null;
    cordovaVersion: string | null;
    regionId: number;
    language: string
    theme: string;
    entityId: number | null;
    loyaltyId: number | null;
    name: string | null;
    lastName: string | null;
    patronymic: string | null;
    email: string | null;
    mobile: string | null;
    dob: string | null;
    sex: string | null;
}
