export default interface RegionType {
    label: string;
    value: number;
}