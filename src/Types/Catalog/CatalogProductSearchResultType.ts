/**
 * @author Medet Bakhytzhan <b.m.bakytzhan@gmail.com>
 */

import ProductSortEnum from 'src/Enums/ProductSortEnum';
import SortValueEnum from 'src/Enums/SortValueEnum';
import CatalogProductType from './CatalogProductType';
import FilterableAttributesType from './FilterableAttributesType';

export default interface CatalogProductSearchResultType {

    totalCount: number;
    count: number;
    page: number;
    products: CatalogProductType[];
    sort: {
        field: ProductSortEnum,
        value: SortValueEnum
    };
    aggregation: {
        minPrice: number,
        maxPrice: number
    };
    filterableAttributes: FilterableAttributesType[] | [];
    filter: any;

}
