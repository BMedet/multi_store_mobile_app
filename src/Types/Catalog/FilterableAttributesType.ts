/**
 * @author Medet Bakhytzhan <b.m.bakytzhan@gmail.com>
 */

export default interface FilterableAttributesType {

    id: number;
    enabled: boolean;
    attributeId: number;
    attributeCode: string;
    frontendLabel: string;
    frontendInput: string;
    isVisibleOnFront: boolean;
    isVisibleOnGrid: boolean;
    isFilterable: boolean;
    isMultiselect: boolean;
    position: number;
    backendType: string;
    options: {
        id?: number;
        attributeId: number;
        optionId: number;
        value: string | null;
        valueId: number;
        sortOrder: number;
        createdAt?: string;
        updatedAt?: string;
    }
}
