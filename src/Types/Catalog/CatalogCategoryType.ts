/**
 * @author Medet Bakhytzhan <b.m.bakytzhan@gmail.com>
 */

export default interface CatalogCategoryType {
    entityId: number;
    name: string;
    urlPath: string;
    thumbnailImage: string | null;
    showThumbnail: boolean;
    parentId: number;
    position: number;
    level: number;
    nameInMenu: string | null;
    includeInMenu: boolean;
    children?: CatalogCategoryType[];
}
