/**
 * @author Medet Bakhytzhan <b.m.bakytzhan@gmail.com>
 */

export default interface CatalogProductType {

    entityId: number;
    name: string;
    sku: string;
    oldPrice: number | null;
    description: string | null;
    thumbnail: string;
    images: string[];
    url: string;
    categoryIds: string;

}
