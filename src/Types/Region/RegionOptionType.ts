/**
 * @author Medet Bakhytzhan <b.m.bakytzhan@gmail.com>
 */

export default interface RegionOptionType {
    entityId: number;
    name: string;
    code: string;
    declensionName: string | null;
    polygon: string | null;
}