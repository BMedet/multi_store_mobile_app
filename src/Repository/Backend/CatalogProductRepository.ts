/**
 * @author Medet Bakhytzhan <b.m.bakytzhan@gmail.com>
 */

import lodash from 'lodash';
import ProductSortEnum from 'src/Enums/ProductSortEnum';
import ProductSourceEnum from 'src/Enums/ProductSourceEnum';
import { currentRegionModel } from 'src/Services/Customer/RegionService';
import CatalogProductSearchResultType from 'src/Types/Catalog/CatalogProductSearchResultType';
import CatalogProductType from 'src/Types/Catalog/CatalogProductType';
import MainRepository from './MainRepository';

export default class CatalogProductRepository extends MainRepository {

    /**
     * Get products by category id
     */
    public async getProducts(source: ProductSourceEnum, id: number, sortField: ProductSortEnum, page: number, filter: any = null): Promise<CatalogProductSearchResultType> {

        // Prepare query
        const query = { page, sortField, filter, regionId: currentRegionModel.value?.value };

        // Make request
        const response = await this.makeRequest(`/catalog/product/${source}/${id}`, 'GET', query);

        const productsFromBackend = lodash.get(response, 'data', []);

        return productsFromBackend;      
    }

    /**
     * Get product by id
     */
    public async getProductById(productId: number): Promise<CatalogProductType> {

        // Prepare query
        const query = { regionId: currentRegionModel.value?.value };

        // Make request
        const response = await this.makeRequest(`/catalog/product/${productId}`, 'GET', query);

        const productsFromBackend = lodash.get(response, 'data', []);

        return productsFromBackend;
    }
}

