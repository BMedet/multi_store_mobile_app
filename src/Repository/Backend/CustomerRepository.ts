/**
 * @author Medet Bakhytzhan <b.m.bakytzhan@gmail.com>
 */

// import lodash from 'lodash';
import MainRepository from './MainRepository';
import InitType from 'src/Types/Customer/InitType';
import CustomerInitType from 'src/Types/Customer/CustomerInitType';

export default class CustomerRepository extends MainRepository {

    /**
     * Init customer
     */
    public async initCustomer(customerData: any): Promise<InitType> {
        const headers = {
            'Content-Type':'application/json'
        };
        // Make request
        const response = await this.makeRequest('/customer', 'POST', null, customerData, headers);

        return response.data;
    }

    /**
     * Init customer
     */
    public async updateCustomerData(customerData: CustomerInitType): Promise<InitType> {
        const headers = {
            'Content-Type':'application/json'
        };
        // Make request
        const response = await this.makeRequest('/customer', 'PUT', null, customerData, headers);

        return response.data;
    }
}

