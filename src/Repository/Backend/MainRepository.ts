import axios, { AxiosError, AxiosInstance } from 'axios';
import appConfig from 'src/config/app';
import BackendRepositoryException from 'src/Exceptions/BackendRepositoryException';

export default class MainRepository {
    // Store id
    STORE_ID = process.env.STORE_ID;

    // Version
    VERSION = 'v1';

    // Bas Url
    BASE_URL = appConfig.backendBaseUrl;
    httpClient: AxiosInstance;
    MAX_SEND_RETRIES = 3;

    /**
     * Constructor
     */
    constructor() {
        this.httpClient = axios.create({
            baseURL: `${this.BASE_URL}/api/${this.VERSION}/${this.STORE_ID}`,
            timeout: 10000
        });
    }

    /**
      * Make request
      */
    protected async makeRequest(url: string, method: 'GET' | 'DELETE' | 'POST' | 'PUT', params: any = null, body: any = null, headers: any = null): Promise<any> {
        try {
            // Make request
            const response = await this.httpClient.request({
                url,
                method,
                params,
                data: body,
                headers
            });

            // Send response, everything is fine
            return response;

        } catch(e) {
            throw new BackendRepositoryException(url, e as AxiosError, '');

        }
    }
}

