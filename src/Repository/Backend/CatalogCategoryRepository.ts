import lodash from 'lodash';
import MainRepository from './MainRepository';

export default class CatalogCategoryRepository extends MainRepository {

    /**
     * Get categories tree
     */
    public async getCategoriesTree(): Promise<any[]> {

        // Make request
        const response = await this.makeRequest('/catalog/category/tree', 'GET');
        return lodash.get(response, 'data', []);

    }


}

