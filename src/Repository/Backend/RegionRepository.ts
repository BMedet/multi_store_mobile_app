import RegionType from '../../Types/RegionType';
import lodash from 'lodash';
import MainRepository from './MainRepository';

export default class RegionRepository extends MainRepository {

    /**
     * Get region options
     */
    public async getRegionOptions(): Promise<RegionType[]> {

        // Make request
        const response = await this.makeRequest('/region', 'GET');
        const regions = lodash.get(response, 'data', []);

        // Typecast
        const preparedRegions = [];
        for(const region of regions) {
            const perRegion: RegionType = {
                label: region.name,
                value: region.entityId
            }
            preparedRegions.push(perRegion);
        }
        return preparedRegions;
    }
}

