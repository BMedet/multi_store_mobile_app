import { ref, Ref } from 'vue';

const routerState: Ref<any> = ref();

export { routerState };