import { QSpinnerGears } from 'quasar';
import { ref } from 'vue';

const $quasarState = ref();

const showLoader = () => {

    $quasarState.value.loading.show({
        spinner: QSpinnerGears,
        spinnerSize: 140,
        backgroundColor: 'black',
        // message: 'Some important process is in progress. Hang on...',
        // messageColor: 'black',
        customClass: 'custom_loader'
    });
}

const hideLoader = () => {
    $quasarState.value.loading.hide();
}

export { showLoader, hideLoader, $quasarState };